const MongoClient = require('mongodb').MongoClient;


const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_auth',
};



const insertData = require('./menuPermissionData.json');

/**
 * Init inserted flag, initial is false
 */
const inserted = {
  // <collection:string>: <true|false>,
};
for(let collection in insertData) {
  inserted[collection] = false;
}


/**
 *
 * ============== CODE base ==============
 *
 */
const insertDocuments = function (db, collection, docs, callback) {
  // Get the documents collection
  const collectionInstance = db.collection(collection);

  // Insert some documents
  collectionInstance.insertMany(docs, function (err, result) {
    console.log(`[${collection}] Inserted ${docs.length} documents into the collection`);
    inserted[collection] = true;

    callback(result);
  });
};

// Use connect method to connect to the server
MongoClient.connect(mongo.url, function (err, client) {
  console.log("Connected successfully to server");

  const db = client.db(mongo.db);

  for (let collection in insertData) {
    const collectionData = insertData[collection];

    insertDocuments(db, collection, collectionData, function (result) {

      const allCollectionWasInserted = Object.keys(inserted).reduce((acc, curr) => acc && inserted[curr], true);
      console.log(`[${collection}] allCollectionWasInserted: `, allCollectionWasInserted);

      if (allCollectionWasInserted) {
        client.close();
      }
    });
  }
});
