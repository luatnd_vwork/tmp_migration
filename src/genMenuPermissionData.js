const { getRandItem } = require('./utils/Array');


let divisionDbData = require('./division.json');
let menuDbData = require('./menu.json');


const existData = {
  division: divisionDbData,
  menu: menuDbData,
  channel: [{ code: 'TVN' }, { code: 'VTN' }, { code: 'VL24' }],
  service: [{ code: 'customer_care' }, { code: 'accountant' }, { code: 'customer_care' }],
};

console.log('divisionDbData: ', divisionDbData.length);
console.log('menuDbData: ', menuDbData.length);

const insertData = {
  permission: [],
  permission_denied: [],
};

const insertDataCount = {
  permission: 10 * 200, // each division might have ~200 action
  permission_denied: 10*10,
};

const commonItemData = {
  active_status: 1,
  created_at: new Date(),
  updated_at: new Date(),
  created_by: null,
  updated_by: null,
  created_source: "luatnd-dev",
};


const exists = {
  'P__division_code__action_code': true,
  'PDENIED__division_code__action_code__channel_code': true,
}


function getCacheKey(type, division_code, action_code, channel_code) {
  return `${type}__${division_code}__${action_code}__${channel_code}`;
}

// Add permission

for (let i = 3000; i <= 3000 + insertDataCount.permission; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001
  const divisionCode = getRandItem(existData.division).code;
  const actionCode = getRandItem(existData.menu).code;

  const item = {
    code: 'PERM_' + numStr,
    division_code: divisionCode,
    action_code: actionCode,
  };


  // Insert only if non exist
  if (typeof exists[getCacheKey('PERM', divisionCode, actionCode)] === 'undefined') {
    insertData.permission.push({ ...item, ...commonItemData });
    exists[getCacheKey('PERM', divisionCode, actionCode)] = true;
  }
}
// Add permission_denied
for (let i = 200; i <= 200 + insertDataCount.permission_denied; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001
  const divisionCode = getRandItem(existData.division).code;
  const actionCode = getRandItem(existData.menu).code;
  const channelCode = getRandItem(existData.channel).code;

  const item = {
    code: 'PERM_D_' + numStr,
    channel_code: channelCode,
    division_code: divisionCode,
    action_code: actionCode,
  };


  // Insert only if non exist
  if (typeof exists[getCacheKey('PERM_D_', divisionCode, actionCode, channelCode)] === 'undefined') {
    insertData.permission_denied.push({ ...item, ...commonItemData });
    exists[getCacheKey('PERM_D_', divisionCode, actionCode, channelCode)] = true;
  }
}


// console.log('insertData: ', insertData);
// process.exit();


var fs = require('fs');
fs.writeFile(
  './src/menuPermissionData.json',
  JSON.stringify(insertData, null, 2),
  function (err) {
    if (err) throw err;
    console.log('Saved to ./menuPermissionData.json');
  }
);

console.log('Finished and exit');


module.exports = insertData;

