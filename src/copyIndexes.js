/**
 * Created by luatnd on 25/7/18.
 */
const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_job',
};

const MongoClient = require('mongodb').MongoClient;

const fs = require('fs')
  , Log = require('log')
  , log = new Log('debug', fs.createWriteStream('logs/my.log'));

log.on('line', function (line) {
  console.log(line);
});



MongoClient.connect(mongo.url, function (err, client) {
  console.log('err: ', err);
  console.log("Connected successfully to server");

  const SRC_DB = 'roma_employer';
  const DST_DB = 'roma_job';
  const SRC_TABLE = 'employer';

  const srcDb = client.db(SRC_DB);
  const dstDb = client.db(DST_DB);

  const srcCollection = srcDb.collection(SRC_TABLE);
  const dstCollection = dstDb.collection('rep_' + SRC_TABLE);

  const tmpIdx = srcCollection.getIndexes();
  console.log('tmpIdx: ', tmpIdx);
  console.log('JSON.stringify(tmpIdx,null,2): ', JSON.stringify(tmpIdx,null,2));
  process.exit();

  const indexes = { dateOfBirth: 1 };

  // Create the index
  dstCollection.createIndex(
    indexes,
    function (err, result) {
      console.log('err: ', err);
      console.log('result: ', result);
    }
  );
});


