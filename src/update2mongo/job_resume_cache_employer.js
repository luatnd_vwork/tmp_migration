/**
 * update increase updated_at +1
 */
const _pick = require('lodash/pick');
const MongoClient = require('mongodb').MongoClient;
const Log = require('../utils/Log');
const Progress = require('../utils/Progress');
const progress = new Progress();
const Int32 = require("mongodb").Int32;
const ObjectId = require("mongodb").ObjectId;


const Config = {
  LogLevel: process.env.LOG_LEVEL,
  MongoUri: process.env.MONGO_URI,
  Size: process.env.SIZE,
}

const mongo = {
  // url: 'mongodb://206.189.150.42:27017',
  // url: 'mongodb://localhost:27017',
  url: Config.MongoUri,
};
const LOG_FILE_NAME_PREFIX = __dirname + '/../../logs/employer';
const DB = {
  roma_employer: 'roma_employer',
  roma_job: 'roma_job',
};
const Collection = {
  employer: 'employer',
  job: 'job',
  job_resume: 'job_resume',
}



let { from = 0, to = 10000 } = _getParam();
from = parseInt(from);
to = parseInt(to);
const LOG_FILE_NAME = `${LOG_FILE_NAME_PREFIX}_${from}_${to}.log`;
const log = new Log(LOG_FILE_NAME);

execute(from, to);

function execute(from, to) {

  MongoClient.connect(mongo.url, async function (err, client) {
    if (err) {
      log.critical("Connect Error: " + err);
      process.exit();
    }

    log.info("Connected successfully to server");
    console.log("Please see the debug info log at: " + LOG_FILE_NAME);


    const size = parseInt(Config.Size);
    const db = client.db(DB.roma_employer);
    const db__roma_job = client.db(DB.roma_job);
    const col = db.collection(Collection.employer);
    const job_resume_col = db__roma_job.collection(Collection.job);

    /**
     * Get total count
     */
    let TOTAL_COUNT = await col.find({}, { _id: 1 }).count();
    log.info("TOTAL_COUNT from db: ", TOTAL_COUNT);

    if (to) {
      TOTAL_COUNT = TOTAL_COUNT < to ? TOTAL_COUNT : to;
      log.info("TOTAL_COUNT param: ", TOTAL_COUNT);
    }

    progress.init(from, TOTAL_COUNT, log);


    await loop(from, size);


    async function loop(from_, size_) {

      //const docs = await getJobItemList(col, from_, size_);
      const docs = await getItemList(col, from_, size_);

      if (docs === null) {
        log.error(`getItemList return null. Please fix it. ${col}, ${from_}, ${size_}`);
        log.error(`Or you might finish the process. Not sure.`);
        return;
      }

      log.info('getItemList count: ', docs.length);


      // Get update data
      const data = [];
      const codes = [];
      docs.map((doc) => {
        const {
          code = null,
          email = null,
          name = null,
          search_name = null,
          address = null,
          contact_address = null,
          contact_email = null,
          phone = null,
          contact_phone = null,
          assigned_staff_code = null,
          account_status = null,
          premium_status = null,
          premium_end_at = null,
          branch_code = null,
        } = doc;

        codes.push(code);

        data.push({
          find: { employer_code: doc.code },
          set: {
            cache_employer: {
              code,
              email,
              name,
              search_name,
              address,
              contact_address,
              contact_email,
              phone,
              contact_phone,
              assigned_staff_code,
              account_status,
              premium_status,
              premium_end_at,
              branch_code,
            }
          },
        });
      });


      const batchResult = await batchUpdate(job_resume_col, data);
      log.info('batchUpdate.result: ' + JSON.stringify(batchResult));
      log.info('+ with job.code list: ' + JSON.stringify(codes), false);


      progress.track(size_);

      if (from_ + size_ < TOTAL_COUNT) {
        log.info('');
        log.info('Continue because from_ < TOTAL_COUNT: ' + (from_ + size_) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from_ + size_, size_), 250);
      } else {
        progress.seek(TOTAL_COUNT);
        log.info(`Finished | From: ${from} | To: ${to}`);

        // Finish up test
        // db.close();
        // db__roma_job.close();
        process.exit(0)
      }
    }

    // When to close db:
    // db.close();
  });
}


async function findCount(col, cond = {}) {
  return new Promise(resolve => {
    col.find(cond).count(function (err, count) {
      if (err) {
        log.error("findCount error: " + err)
      }

      resolve(count);
    });
  });
}


function getCommonResult(result) {
  return _pick(result, [
    'matchedCount', 'modifiedCount', 'upsertedCount', 'upsertedId',
    'nInserted', 'nUpserted', 'nMatched', 'nModified'
  ])
}


function _getParam() {
  let from = typeof process.argv[2] !== 'undefined' ? process.argv[2] : 0;
  let to = typeof process.argv[3] !== 'undefined' ? process.argv[3] : 0;

  return { from, to }
}


function batchUpdate(col, data) {
  var batch = col.initializeUnorderedBulkOp({useLegacyOps: true});

  // Add some operations to be executed in order
  data.map(updateData => {
    const updateDocument = {
      $set: updateData.set,
      $inc: { sync_tid: Int32(1) },
    };

    batch.find(updateData.find).update(updateDocument);

    // log.debug('Update data: ' + JSON.stringify({
    //     filter: updateData.find,
    //     updateDocument
    //   }), false);

    return true;
  });

  // Execute the operations
  return new Promise(resolve => {
    batch.execute(function (err, result) {
      resolve({ err, result })
    });
  });
}


async function getItemList(col, from, size) {

  let cur = await col.find({}, {
    _id: 1,
    "email": 1,
    "name": 1,
    "search_name": 1,
    "address": 1,
    "contact_address": 1,
    "contact_email": 1,
    "phone": 1,
    "contact_phone": 1,
    "assigned_staff_code": 1,
    "account_status": 1,
    "premium_status": 1,
    "premium_end_at": 1,
    "branch_code": 1,
  }).sort({ _id: 1 }).skip(from).limit(size).maxTimeMS(600000);

  const items = await new Promise(resolve => {
    cur.toArray(function (err, result) {
      if (err) {
        throw new Error(err)
      }
      // cur.close();

      resolve(result);
    });
  });

  return items;
}
