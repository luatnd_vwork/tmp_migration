/**
 * Migrate mongo2mongo
 * Support both table2table and table2field case
 *
 * Usage:
 *    Step 1: setting your SRC_TABLE, DST_TABLES
 *    Step2: source .env && node src/update2mongo/mongo2mongo.js 0 9999999
 *
 *
 *      OR
 *      source .env
 *      nohup node src/update2mongo/mongo2mongo.js 0 9999999 &
 *
 * Further improvement:
 *    [ ] Use range based instead of skip + limit
 *    [ ] Use async if it's needed
 */

const _pick = require('lodash/pick');
const MongoClient = require('mongodb').MongoClient;
const Log = require('../utils/Log');
const ArrayUtil = require('../utils/Array');
const Progress = require('../utils/Progress');
const Slack = require('../client/slack');
const progress = new Progress();
const Int32 = require("mongodb").Int32;
const ObjectId = require("mongodb").ObjectId;
const moment = require("moment");



/**
 * App Config
 */
const Config = {
  Env: process.env.ENV,
  LogLevel: process.env.LOG_LEVEL,
  MongoUri: process.env.MONGO_URI,
  Size: process.env.SIZE,

  SrcTable: process.env.SRC_TABLE,
  DstTables: JSON.parse(process.env.DST_TABLES),
  CreateIndexes: process.env.CREATE_INDEXES,

  SchemaMap: (
    typeof process.env.SCHEMA_MAP !== 'undefined'
      ? JSON.parse(process.env.SCHEMA_MAP)
      : null
  )
};


// TODO: Make ENV var just like the kafka2mongo module
const MongoConfig = {
  // url: 'mongodb://206.189.150.42:27017',
  // url: 'mongodb://localhost:27017',
  url: Config.MongoUri,
};
const LOG_FILE_DIR = __dirname + '/../../logs';
const LOG_FILE_NAME_PREFIX = `${Config.SrcTable}_${moment().format('YYMMDDhhmm')}_`;


/**
 * Bootstrap
 */
let { from = 0, to = 0 } = _getParam();
from = parseInt(from);
to = parseInt(to);
const LOG_FILE_NAME = `${LOG_FILE_DIR}/${LOG_FILE_NAME_PREFIX}_${from}_${to}.log`;
const log = new Log(LOG_FILE_NAME);


if (Config.Env === 'dev') {
  log.debug("ENV=dev ==> Load default SchemaMap from mock data.");
  Config.SchemaMap = require('../const/SchemaMap');
} else {
  if (Config.SchemaMap === null) {
    log.critical("No SCHEMA_MAP env var");
    process.exit()
  }
}


if (!from && !to) {
  throw new Error('From and To param is required. Eg: node xxx.js 0 1000');
}


/**
 * Main program
 */
try {
  main(from, to);
}
catch (e) {
  Slack.critical("Error kìa:" + e.message);

  throw e;
}

function main(from, to) {
  const DstConfigs = Config.DstTables.length
    ? _pick(Config.SchemaMap[Config.SrcTable], Config.DstTables)
    : Config.SchemaMap[Config.SrcTable];

  if (!DstConfigs) {
    log.critical('No schemap configuration for ' + Config.SrcTable);
    process.exit()
  }

  log.info('༼ つ ◕_◕ ༽つ Start migrating from `' + Config.SrcTable + '` to ' + JSON.stringify(Object.keys(DstConfigs)));


  MongoClient.connect(MongoConfig.url, async function (err, client) {
    if (err) {
      log.critical("Connect Error: " + err);
      process.exit();
    }

    log.info("Connected successfully to server");
    console.log("Please see the debug info log at: " + LOG_FILE_NAME);

    const size = parseInt(Config.Size);

    //const src_db = client.db(DB.roma_employer);
    const src_coll = getCollection(client, Config.SrcTable);

    /**
     * Get total count
     */
    let TOTAL_COUNT = await src_coll.find({}, { _id: 1 }).count();
    log.info("TOTAL_COUNT from src_db: " +  TOTAL_COUNT);

    if (to) {
      TOTAL_COUNT = TOTAL_COUNT < to ? TOTAL_COUNT : to;
      log.info("TOTAL_COUNT param: " + TOTAL_COUNT);
    }

    progress.init(from, TOTAL_COUNT, log);


    // Create indexes
    if (Config.CreateIndexes.toString() === "1") {
      createIndexesIfNeeded(client, DstConfigs);
    }

    // Do bulk migrating data
    await loop(from, null, size);

    async function loop(from_, last_id_, size_) {
      log.info(`loop: from=${from_}, batch_size=${size_}`);

      if (from_ > TOTAL_COUNT) {
        progress.seek(TOTAL_COUNT);
        log.info(`Finished | From: ${from} | To: ${to}`);

        // Finish up test
        Slack.info("Finished");
        process.exit(0)
      }

      //const docs = await getJobItemList(src_coll, from_, size_);
      // List of target collection fields, we ignore other fields
      const fields = [];
      const docs = await getItemList(src_coll, fields, from_, last_id_, size_);

      if (docs === null) {
        log.error(`getItemList return null. Please fix it. ${src_coll}, ${from_}, ${size_}`);
        log.error(`Or you might finish the process. Not sure.`);

        log.info('');
        log.info('Retry last failed loop: ' + (from_) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from_, last_id_, size_), 250);

        return;
      }

      log.info('getItemList count: ' + docs.length);


      for (let dstTableFullPath in DstConfigs) {
        const dstConf = DstConfigs[dstTableFullPath];
        const dst_coll = getCollection(client, dstTableFullPath);

        if (Array.isArray(dstConf)) {
          migrateTable2Table(dstConf, dst_coll, docs);
        } else {
          migrateTable2Field(dstConf, dst_coll, docs);
        }
      }

      progress.track(size_);


      log.info('');
      log.info('Continue: ' + JSON.stringify({ from_, size_, TOTAL_COUNT, }));

      const last_doc = docs[docs.length - 1];
      setTimeout(() => loop(from_ + size_, last_doc._id, size_), 250);
    }
  });
}


async function findCount(col, cond = {}) {
  return new Promise(resolve => {
    col.find(cond).count(function (err, count) {
      if (err) {
        log.error("findCount error: " + err)
      }

      resolve(count);
    });
  });
}


function getCommonResult(result) {
  return _pick(result, [
    'ok',
    'matchedCount', 'modifiedCount', 'upsertedCount',
    'upsertedId', 'upsertedIds',
    'nInserted', 'nUpserted', 'nMatched', 'nModified', 'nRemoved',
  ])
}


function _getParam() {
  let from = typeof process.argv[2] !== 'undefined' ? process.argv[2] : 0;
  let to = typeof process.argv[3] !== 'undefined' ? process.argv[3] : 0;

  return { from, to }
}


function batchUpdate(col, data, upsert = false) {
  const batch = col.initializeUnorderedBulkOp({useLegacyOps: true});

  // Add some operations to be executed in order
  data.map(updateData => {
    const updateDocument = {
      $set: updateData.set,
      $inc: { sync_tid: Int32(1) },
    };

    if (upsert) {
      batch.find(updateData.find).upsert().update(updateDocument);
    } else {
      batch.find(updateData.find).update(updateDocument);
    }


    // log.debug('Update data: ' + JSON.stringify({
    //     filter: updateData.find,
    //     updateDocument
    //   }), false);

    return true;
  });

  // Execute the operations
  return new Promise(resolve => {
    batch.execute(function (err, result) {
      resolve({ err, result })
    });
  });
}


async function getItemList(col, fields, from, last_id, size) {
  const canUseIdxScan = last_id !== null;

  const pipeline = [];

  // const project = {};
  // const project = { _id: 1 };
  // for (let i in fields) {
  //   const f = fields[i];
  //   if (f !== 'id') {
  //     project[f] = 1;
  //   }
  // }

  const match = canUseIdxScan ? { _id: { $gt: ObjectId(last_id) } } : {};

  if (canUseIdxScan) pipeline.push({ $match: match });
  pipeline.push({ $sort: { _id: 1 } });
  if (!canUseIdxScan) pipeline.push({ $skip: from });
  pipeline.push({ $limit: size });
  // pipeline.push({ $project: project });

  let cur = await col.aggregate(pipeline, { allowDiskUse: true });

  const items = await new Promise(resolve => {
    cur.toArray(function (err, result) {
      if (err) {
        log.error("getItemList ERROR: " + JSON.stringify(err));
        throw new Error(err)
      }
      // cur.close();

      resolve(result);
    });
  });

  return items;
}


async function migrateTable2Table(dstConf, dst_coll, docs) {
  const src_fields = transformFields(dstConf);
  const defaultData = ArrayUtil.toObject(src_fields, i => i, i => null);

  // Get update data
  const data = [];
  const src_table_codes = [];
  docs.map((doc) => {
    const findCond = { '_id': doc._id };
    const setData = { ...defaultData, ..._pick(doc, src_fields) };

    src_table_codes.push(doc.code);

    data.push({
      find: findCond,
      set: setData,
    });
  });


  const batchResult = await batchUpdate(dst_coll, data, true);
  log.info('batchUpdate.result: ' + JSON.stringify(getCommonResult(batchResult.result)));
  log.info(`+ with ${Config.SrcTable}.code list: ` + JSON.stringify(src_table_codes), false);

}
async function migrateTable2Field(dstConf, dst_coll, docs) {
  const {
    join_src_field = '',
    join_dst_field = '',
    src_fields:src_f = [],
    dst_field = '',
  } = dstConf;

  const src_fields = transformFields(src_f);
  const defaultData = ArrayUtil.toObject(src_fields, i => i, i => null);

  // Get update data
  const data = [];
  const src_table_codes = [];
  docs.map((doc) => {
    const dstFieldData = { ...defaultData, ..._pick(doc, src_fields) };

    const join_src_field_value = doc[join_src_field];
    src_table_codes.push(doc.code);

    const findCond = { [join_dst_field]: join_src_field_value };
    const setData = { [dst_field]: dstFieldData };

    data.push({
      find: findCond,
      set: setData,
    });
  });


  const batchResult = await batchUpdate(dst_coll, data);
  log.info('batchUpdate.result: ' + JSON.stringify(getCommonResult(batchResult.result)));
  log.info(`+ with ${Config.SrcTable}.code list: ` + JSON.stringify(src_table_codes), false);
}


async function createIndexesIfNeeded(mongoClient, DstConfigs) {
  for (let dstTableFullPath in DstConfigs) {
    const dstConf = DstConfigs[dstTableFullPath];

    if (Array.isArray(dstConf)) {
      // Create index for rep_* table:
      // Ignore this case because you don't have enough information
      continue;
    } else {
      // Create index for dst_table.cache_*.dst_field
      const dst_coll = getCollection(mongoClient, dstTableFullPath);
      const indexSpecs = dstConf.dst_field_indexes.map(f => ({ [f]: 1 }));

      await new Promise(resolve => {
        log.info(`Creating indexes for '${dstTableFullPath}': col.createIndexes(${JSON.stringify(indexSpecs)}, { background: true }`);

        dst_coll.createIndexes(indexSpecs, { background: true }, (err, result) => {
          if (err) {
            log.error("Create indexes ERROR: " + JSON.stringify(err))
          } else {
            log.info("Create indexes SUCCESS" + JSON.stringify(result))
          }

          resolve({ err, result })
        });
      });
    }
  }
}


function getTableInfo(dstTableFullPath) {
  const s = dstTableFullPath.split('.');
  return {
    db: s[0],
    table: s[1],
  }
}

function getCollection(mongoClient, tableFullPath) {
  const t = getTableInfo(tableFullPath);
  return mongoClient.db(t.db).collection(t.table);
}

function transformFields(fields) {
  const fieldMap = {
    'id': '_id',
  };

  return fields.filter(f => !(f in fieldMap));
}