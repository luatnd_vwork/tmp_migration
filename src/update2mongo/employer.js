/**
 * update increase updated_at +1
 */
const _pick = require('lodash/pick');

const mongo = {
  url: 'mongodb://206.189.150.42:27017',
};
const LOG_FILE_NAME = __dirname + '/../../logs/employer.log';
const DB = {
  roma_employer: 'roma_employer',
};
const Collection = {
  employer: 'employer',
}


const MongoClient = require('mongodb').MongoClient;
const Log = require('../utils/Log');
const log = new Log(LOG_FILE_NAME);
const Progress = require('../utils/Progress');
const progress = new Progress();


log.on('line', function(line){
  console.log(line);
});

const Int32 = require("mongodb").Int32;
const ObjectId = require("mongodb").ObjectId;


execute();

function execute() {

  MongoClient.connect(mongo.url, async function (err, client) {

    log.info("Connected successfully to server");
    console.log("Please see the debug info log at: " + LOG_FILE_NAME);

    const { from = 0, to = 10000 } = _getParam();

    const size = 100;
    const db = client.db(DB.roma_employer);
    const col = db.collection(Collection.employer);

    let TOTAL_COUNT = await col.find({}, { _id: 1 }).count();
    console.log("\nTOTAL_COUNT from db: ", TOTAL_COUNT);

    if (to) {
      TOTAL_COUNT = to;
      console.log("\nTOTAL_COUNT param: ", TOTAL_COUNT);
    }

    progress.init(TOTAL_COUNT, log);

    let result = await col.updateMany(
      { updated_at: null },
      { $set: { updated_at: Int32(0) } }
    );
    console.log('result set updated_at null to 0 to increase: ' + "\n", getCommonResult(result));

    await loop(from, size);

    async function loop(from, size) {
      let cur = await col.find({}, { _id: 1 }).skip(from).limit(size).maxTimeMS(600000);

      const docIds = await new Promise(resolve => {
        cur.toArray(function(err, result) {
          if (err) {
            throw new Error(err)
          }
          cur.close();

          resolve(result.map(doc => ObjectId(doc._id)));
        });
      });

      let result = await col.updateMany(
        {
          _id: { $in: docIds },
          updated_at: { $ne: null },
        },
        { $inc: { updated_at: Int32(1) } }
      )
      const msg = 'batchUpdate.result: ' + JSON.stringify(getCommonResult(result));
      console.log(msg);
      log.info(msg);

      progress.track(from);

      if (from + size < TOTAL_COUNT) {
      	log.info('');
        log.info('Continue because from < TOTAL_COUNT: ' + (from + size) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from + size, size), 250);
      } else{
        Progress.track(TOTAL_COUNT);
        log.info('Finished. TOTAL_COUNT: ' + TOTAL_COUNT);
      }
    }

    // When to close db:
    // db.close();
  });
}


async function findCount(col, cond = {}) {
  return new Promise(resolve => {
    col.find(cond).count(function (err, count) {
      if (err) {
        console.log('err: ', err);
        log.error("findCount error: " + err)
      }

      resolve(count);
    });
  });
}


function getCommonResult(result) {
  return _pick(result, ['matchedCount', 'modifiedCount', 'upsertedCount', 'upsertedId'])
}


function _getParam() {
  console.log('process.argv: ', process.argv);
  let from = 0;

  // process.exit()

  return {
    from: 0,
    to: 1,
  }
}