/**
 * update increase updated_at +1
 */
const _pick = require('lodash/pick');
const MongoClient = require('mongodb').MongoClient;
const Log = require('../utils/Log');
const Progress = require('../utils/Progress');
const progress = new Progress();
const Int32 = require("mongodb").Int32;
const ObjectId = require("mongodb").ObjectId;


const Config = {
  LogLevel: process.env.LOG_LEVEL,
  MongoUri: process.env.MONGO_URI,
  Size: process.env.SIZE,
}

const mongo = {
  // url: 'mongodb://206.189.150.42:27017',
  // url: 'mongodb://localhost:27017',
  url: Config.MongoUri,
};
const LOG_FILE_NAME_PREFIX = __dirname + '/../../logs/sales_order';
const DB = {
  roma_employer: 'roma_employer',
  roma_job: 'roma_job',
  roma_sales_order: 'roma_sales_order',
};
const Collection = {
  employer: 'employer',
  job: 'job',
  sales_order: 'sales_order',
}



let { from = 0, to = 0 } = _getParam();
from = parseInt(from);
to = parseInt(to);
const LOG_FILE_NAME = `${LOG_FILE_NAME_PREFIX}_${from}_${to}.log`;
const log = new Log(LOG_FILE_NAME);

if (!from && !to) {
  throw new Error('From and To param is required. Eg: node xxx.js 0 1000');
}

execute(from, to);

function execute(from, to) {

  MongoClient.connect(mongo.url, async function (err, client) {
    if (err) {
      log.critical("Connect Error: " + err);
      process.exit();
    }

    log.info("Connected successfully to server");
    console.log("Please see the debug info log at: " + LOG_FILE_NAME);


    const size = parseInt(Config.Size);

    const db__employer = client.db(DB.roma_employer);
    const emp_coll = db__employer.collection(Collection.employer);

    const db__sales_order = client.db(DB.roma_sales_order);
    const so_coll = db__sales_order.collection(Collection.sales_order);

    /**
     * Get total count
     */
    let TOTAL_COUNT = await emp_coll.find({}, { _id: 1 }).count();
    log.info("TOTAL_COUNT from db__employer: " +  TOTAL_COUNT);

    if (to) {
      TOTAL_COUNT = TOTAL_COUNT < to ? TOTAL_COUNT : to;
      log.info("TOTAL_COUNT param: " + TOTAL_COUNT);
    }

    progress.init(from, TOTAL_COUNT, log);


    await loop(from, size);


    async function loop(from_, size_) {
      log.info(`loop: from=${from_}, batch_size=${size_}`);

      //const docs = await getJobItemList(emp_coll, from_, size_);
      const docs = await getItemList(emp_coll, from_, size_);

      if (docs === null) {
        log.error(`getItemList return null. Please fix it. ${emp_coll}, ${from_}, ${size_}`);
        log.error(`Or you might finish the process. Not sure.`);

        log.info('');
        log.info('Retry last failed loop: ' + (from_ + size_) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from_ + size_, size_), 250);

        return;
      }

      log.info('getItemList count: ' +  docs.length);


      // Get update data
      const data = [];
      const codes = [];
      docs.map((doc) => {
        const {
          code = null,
          incremental_id = null,
          branch_code = null,
          assigned_staff_code = null,
          name = null,
          search_name = null,
          email = null,
        } = doc;

        codes.push(code);

        data.push({
          find: { employer_code: doc.code },
          set: {
            cache_employer: {
              code,
              incremental_id,
              branch_code,
              assigned_staff_code,
              name,
              search_name,
              email,
            }
          },
        });
      });


      const batchResult = await batchUpdate(so_coll, data);
      log.info('batchUpdate.result: ' + JSON.stringify(batchResult));
      log.info('+ with job.code list: ' + JSON.stringify(codes), false);


      progress.track(size_);

      if (from_ + size_ < TOTAL_COUNT) {
        log.info('');
        log.info('Continue because from_ < TOTAL_COUNT: ' + (from_ + size_) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from_ + size_, size_), 250);
      } else {
        progress.seek(TOTAL_COUNT);
        log.info(`Finished | From: ${from} | To: ${to}`);

        // Finish up test
        // db__employer.close();
        // db__sales_order.close();
        process.exit(0)
      }
    }

    // When to close db__employer:
    // db__employer.close();
  });
}


async function findCount(col, cond = {}) {
  return new Promise(resolve => {
    col.find(cond).count(function (err, count) {
      if (err) {
        log.error("findCount error: " + err)
      }

      resolve(count);
    });
  });
}


function getCommonResult(result) {
  return _pick(result, [
    'matchedCount', 'modifiedCount', 'upsertedCount', 'upsertedId',
    'nInserted', 'nUpserted', 'nMatched', 'nModified'
  ])
}


function _getParam() {
  let from = typeof process.argv[2] !== 'undefined' ? process.argv[2] : 0;
  let to = typeof process.argv[3] !== 'undefined' ? process.argv[3] : 0;

  return { from, to }
}


function batchUpdate(col, data) {
  var batch = col.initializeUnorderedBulkOp({useLegacyOps: true});

  // Add some operations to be executed in order
  data.map(updateData => {
    const updateDocument = {
      $set: updateData.set,
      $inc: { sync_tid: Int32(1) },
    };

    batch.find(updateData.find).update(updateDocument);

    // log.debug('Update data: ' + JSON.stringify({
    //     filter: updateData.find,
    //     updateDocument
    //   }), false);

    return true;
  });

  // Execute the operations
  return new Promise(resolve => {
    batch.execute(function (err, result) {
      resolve({ err, result })
    });
  });
}


async function getItemList(col, from, size) {

  let cur = await col.find({}, {
    _id: 1,
    "code": 1,
    "incremental_id": 1,
    "branch_code": 1,
    "assigned_staff_code": 1,
    "name": 1,
    "search_name": 1,
    "email": 1,
  }).sort({ _id: 1 }).skip(from).limit(size).maxTimeMS(600000);

  const items = await new Promise(resolve => {
    cur.toArray(function (err, result) {
      if (err) {
        log.error("getItemList ERROR: " + JSON.stringify(err));
        throw new Error(err)
      }
      // cur.close();

      resolve(result);
    });
  });

  return items;
}
