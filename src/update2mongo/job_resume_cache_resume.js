const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_job',
};
const LOG_FILE_NAME = 'logs/job_resume_cache_resume.log';


const MongoClient = require('mongodb').MongoClient;
const fs = require('fs')
  , Log = require('log')
  , log = new Log('debug', fs.createWriteStream(LOG_FILE_NAME));


log.on('line', function(line){
  console.log(line);
});

execute();

function execute() {
  const TOTAL_COUNT = 17258994;

  MongoClient.connect(mongo.url, function (err, client) {

    log.info("Connected successfully to server");

    let from = 0;
    const size = 5000;
    const db = client.db(mongo.db);
    const col = db.collection('job_resume');

    loop(from, size);


    async function loop(from, size) {

      //const docs = await getJobItemList(col, from, size);
      const docs = await getItemList(col, from, size);

      if (docs === null) {
        log.error(`getJobItemList return null. Please fix it. ${col}, ${from}, ${size}`);
        log.error(`Or you might finish the process. Not sure.`);
        return;
      }

      log.info('getJobItemList count: ', docs.length);

      // Get update data
      const data = [];
      docs.map((doc) => {
        const {
          title = null,
          resume_type = null,
          updated_at = null,
          job_field_codes = null,
          province_codes = null,
          resume_status = null,
        } = (typeof doc.resume[0] !== 'undefined') ? doc.resume[0] : {};

        data.push({
          find: { _id: doc._id },
          set: {
            cache_resume: {
              title,
              resume_type,
              updated_at,
              job_field_codes,
              province_codes,
              resume_status,
            }
          },
        });
      });

      const result = await batchUpdate(col, data);
      log.info('batchUpdate.result: ', result);


      if (from + size < TOTAL_COUNT) {
      	log.info('');
        log.info('Continue because from < TOTAL_COUNT: ' + (from + size) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from + size, size), 250);
      } else{
        log.info('Finished. TOTAL_COUNT: ' + TOTAL_COUNT);
      }
    }

    console.log("Please see the: " + LOG_FILE_NAME);

    // When to close db:
    // db.close();
  });
}

function getItemList(col, from, size) {
  return new Promise(resolve => {
    const pipeline = [
      { $skip: from },
      { $limit: size },
      {
        $lookup: {
          from: 'rep_resume',
          localField: 'resume_code',
          foreignField: 'code',
          as: 'resume',
        }
      },
      {
        $project: {
          _id: 1,
          resume_code: 1,
          resume: {
            title: 1,
            resume_type: 1,
            updated_at: 1,
            job_field_codes: 1,
            province_codes: 1,
            resume_status: 1,
          }
        }
      },
    ];

    col.aggregate(pipeline).toArray(function (err, docs) {
      if (docs === null) {
        log.error('aggregate.toArray return null with aggregation pipeline: ' + JSON.stringify(pipeline));
        log.error(JSON.stringify(err));
        console.log(err);
      }

      resolve(docs);

      //db.close();
    });
  });
}

function batchUpdate(col, data) {
  const batch = col.initializeUnorderedBulkOp({ useLegacyOps: true });

  // Add some operations to be executed in order
  data.map(updateData => {
    batch.find(updateData.find).updateOne({ $set: updateData.set });

    return true;
  });

  // Execute the operations
  return new Promise(resolve => {
    batch.execute(function (err, result) {
      resolve({
        success: true,
        error: err,
        //result: result,
      })
    });
  });
}