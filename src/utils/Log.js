const fs = require('fs')
  , Logger = require('log');

const moment = require('moment');

class Log {
  // LOG_FILE_NAME = '';
  // logger = null;

  constructor(logFileName) {
    this.init(logFileName)
  }

  init(logFileName) {
    this.LOG_FILE_NAME = logFileName;
    this.logger = new Logger('debug', fs.createWriteStream(this.LOG_FILE_NAME));
  }

  debug(msg, cl = true) {
    cl && console.log(msg)
    msg = Log.formatMessage(msg)
    this.logger.debug(msg)
  }

  info(msg, cl = true) {
    cl && console.log(msg)
    msg = Log.formatMessage(msg)
    this.logger.info(msg)
  }

  warn(msg, cl = true) {
    cl && console.log(msg)
    msg = Log.formatMessage(msg)
    this.logger.warn(msg)
  }

  error(msg, cl = true) {
    cl && console.log(msg)
    msg = Log.formatMessage(msg)
    this.logger.error(msg)
  }

  critical(msg, cl = true) {
    cl && console.log(msg)
    msg = Log.formatMessage(msg)
    this.logger.critical(msg)
  }

  static formatMessage(msg) {
    // return '[' + moment().format() + '] ' + msg;
    return msg;
  }

  on(event, cb) {
    this.logger.on(event, cb);
  }
}

module.exports = Log;