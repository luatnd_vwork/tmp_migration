const getRandItem = (items) => items[Math.floor(Math.random() * items.length)];

/**
 * Tranform array into object
 * Usage:
 *    ArrayUtil.toObject(fields, i => i, i => null);
 *
 * @param array
 * @param keyGetter
 * @param valueGetter
 * @returns {{}}
 */
const toObject = (array, keyGetter, valueGetter) => {
  const obj = {};
  for (let i in array) {
    let e = array[i];
    obj[keyGetter(e)] = valueGetter(e);
  }
  return obj;
};

module.exports = {
  getRandItem,
  toObject,
}