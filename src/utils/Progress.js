class Progress {
  // total = 1;
  // currentCount = 0;
  // logger = null;
  // created_at_ms = null;

  constructor(total = 1, logger = null) {
    this.init(total, logger)
  }

  init(from, to, logger) {
    this.total = to - from;
    this.from = from;
    this.to = to;
    this.currentCount = 0;
    this.logger = logger;
    this.created_at_ms = new Date().getTime();
    this.now_ms = new Date().getTime();
  }

  seek(position) {
    this.currentCount = position - this.from;
    this.now_ms = new Date().getTime();

    this.report();
  }

  track(delta) {
    this.currentCount += delta;

    if (this.currentCount > this.total) {
      this.currentCount = this.total;
    }

    this.now_ms = new Date().getTime();

    this.report();
  }

  report() {
    const percent = Math.floor(this.currentCount / this.total * 100)

    const elapseUnixMs = this.now_ms - this.created_at_ms;
    const elapseTime = new Date(elapseUnixMs);
    const estimateTime = new Date(elapseUnixMs / this.currentCount * this.total);

    const msg = `Progress ${percent}% | ${this.currentCount}/${this.total} | Elapse: ${Progress._getTimeStr(elapseTime)} | Estimate: ${Progress._getTimeStr(estimateTime)}`;

    this.logger.info(msg);
  }

  static _getTimeStr(dateTime) {
    const hh = dateTime.getUTCHours().toString().padStart(2, '0');
    const mm = dateTime.getUTCMinutes().toString().padStart(2, '0');
    const ss = dateTime.getUTCSeconds().toString().padStart(2, '0');

    return `${hh}:${mm}:${ss}`;
  }
}

module.exports = Progress;