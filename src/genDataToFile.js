const { getRandItem } = require('./utils/Array');

const insertData = {
  division: [],
  data_group: [],
  action: [],
  staff: [],
  permission: [],
  permission_denied: [],
  menu: [],
};

const insertDataCount = {
  division: 10,
  data_group: 50,
  action: 300,
  staff: 100,
  permission: 10 * 100, // each division might have ~200 action
  permission_denied: 100,
  menu: 0,
}

const commonItemData = {
  active_status: 1,
  created_at: new Date(),
  updated_at: new Date(),
  created_by: null,
  updated_by: null,
  created_source: "luatnd-dev",
};

const channels = [{ code: 'TVN' }, { code: 'VTN' }, { code: 'VL24' }];



// Add division
for (let i = 1; i <= insertDataCount.division; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'DIV_' + numStr,
    short_name: 'short_name ' + numStr,
    full_name: 'full_name ' + numStr,
    _ref_roleId: '_ref_roleId ' + numStr,
  };

  insertData.division.push({ ...item, ...commonItemData });
}
// Add data_group
for (let i = 1; i <= insertDataCount.data_group; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'DG_' + numStr,
    name: 'name ' + numStr,
    branch_codes: getRandItem(['MB', 'MN', null]),
  };

  insertData.data_group.push({ ...item, ...commonItemData });
}
// Add action
for (let i = 1; i <= insertDataCount.action; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'ACT_' + numStr,
    action_group: 'action_group ' + numStr,
    name: 'name ' + numStr,
    service: 'service ' + numStr,
    controller: 'controller_' + numStr,
    action: 'action_' + numStr,
  };

  insertData.action.push({ ...item, ...commonItemData });
}
// Add staff
for (let i = 1; i <= insertDataCount.staff; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: "STAFF_" + numStr,
    channel_code: getRandItem(channels).code,
    division_code: getRandItem(insertData.division).code,
    login_name: 'luatnd_' + numStr,
    password: 'luatnd_' + numStr,
    display_name: 'Luat Nguyen ' + numStr,
    avatar_path: 'avatar ' + numStr,
    address: 'address ' + numStr,
    phone: 'phone ' + numStr,
    email: 'email ' + numStr,
    data_group_code: getRandItem(insertData.data_group).code,
    language_code: getRandItem(['vi-vn', 'en-us']),
    xlite_id: 'xlite_id ' + numStr,
    start_working_date: 'start_working_date ' + numStr,
    end_working_date: 'end_working_date ' + numStr,
    _ref_userId: '_ref_userId ' + numStr,
    _ref_roleId: '_ref_roleId ' + numStr,
  };

  insertData.staff.push({ ...item, ...commonItemData });
}
// Add permission
for (let i = 1; i <= insertDataCount.permission; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'PERM_' + numStr,
    division_code: getRandItem(insertData.division).code,
    action_code: getRandItem(insertData.action).code,
  };

  insertData.permission.push({ ...item, ...commonItemData });
}
// Add permission_denied
for (let i = 1; i <= insertDataCount.permission_denied; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'PERM_D_' + numStr,
    channel_code: getRandItem(channels).code,
    division_code: getRandItem(insertData.division).code,
    action_code: getRandItem(insertData.action).code,
  };

  insertData.permission_denied.push({ ...item, ...commonItemData });
}


// Add menu
const menus = require('./menus.json');
menus.map(item => {
  insertData.menu.push({ ...item, ...commonItemData });
});

//console.log('insertData: ', insertData);

var fs = require('fs');
fs.writeFile(
  './src/insertData.json',
  JSON.stringify(insertData, null, 2),
  function (err) {
    if (err) throw err;
    console.log('Saved to ./insertData.json');
  }
);

console.log('Finished and exit');


module.exports = insertData;

