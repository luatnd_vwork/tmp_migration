module.exports = {
  "roma_auth.staff": {
    "roma_system.rep_staff": [
      "id",
      "code",
      "channel_code",
      "login_name",
      "display_name",
      "email",
      "division_code",
      "data_group_code",
      "start_working_date",
      "end_working_date",
      "active_status"
    ],
    "roma_employer.rep_staff": [
      "id",
      "code",
      "channel_code",
      "display_name",
      "phone",
      "email",
      "xlite_id",
      "division_code",
      "data_group_code",
      "language_code",
      "active_status"
    ],
    "roma_job.rep_staff": [
      "id",
      "code",
      "channel_code",
      "display_name",
      "division_code",
      "data_group_code",
      "email",
      "active_status"
    ],
    "roma_sales_order.rep_staff": [
      "id",
      "code",
      "channel_code",
      "display_name",
      "division_code",
      "data_group_code",
      "email",
      "active_status",
      "phone"
    ],
    "roma_booking.rep_staff": [
      "code",
      "data_group_code",
      "division_code",
      "email",
      "active_status"
    ],
    "roma_planning.rep_staff": [
      "id",
      "code",
      "channel_code",
      "login_name",
      "display_name",
      "email",
      "division_code",
      "data_group_code",
      "start_working_date",
      "end_working_date",
      "active_status"
    ],
    "roma_call_history.rep_staff": [
      "id",
      "code",
      "channel_code",
      "login_name",
      "display_name",
      "email",
      "division_code",
      "data_group_code",
      "active_status"
    ],
    "roma_accountant.rep_staff": [
      "id",
      "code",
      "channel_code",
      "login_name",
      "display_name",
      "email",
      "division_code",
      "data_group_code",
      "active_status"
    ]
  },
  "roma_auth.data_group": {
    "roma_system.rep_data_group": [
      "id",
      "code",
      "name",
      "branch_codes",
      "active_status"
    ],
    "roma_employer.rep_data_group": [
      "id",
      "code",
      "name",
      "branch_codes",
      "active_status"
    ],
    "roma_job.rep_data_group": [
      "id",
      "code",
      "branch_codes"
    ],
    "roma_sales_order.rep_data_group": [
      "id",
      "code",
      "branch_codes"
    ],
    "roma_booking.rep_data_group": [
      "id",
      "code",
      "branch_codes",
      "name",
      "active_status"
    ],
    "roma_planning.rep_data_group": [
      "id",
      "code",
      "name",
      "branch_codes",
      "active_status"
    ],
    "roma_call_history.rep_data_group": [
      "id",
      "code",
      "name",
      "branch_codes",
      "active_status"
    ]
  },
  "roma_auth.team": {
    "roma_employer.rep_team": [
      "id",
      "code",
      "name",
      "branch_code",
      "type",
      "active_status"
    ],
    "roma_job.rep_team": [
      "id",
      "code",
      "name",
      "branch_code",
      "type",
      "active_status"
    ],
    "roma_booking.rep_team": [
      "id",
      "code",
      "branch_code",
      "active_status"
    ],
    "roma_planning.rep_team": [
      "id",
      "code",
      "branch_code",
      "name",
      "type",
      "active_status"
    ],
    "roma_call_history.rep_team": [
      "id",
      "code",
      "branch_code",
      "name",
      "type",
      "active_status"
    ]
  },
  "roma_auth.team_member": {
    "roma_employer.rep_team_member": [
      "id",
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ],
    "roma_job.rep_team_member": [
      "id",
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ],
    "roma_sales_order.rep_team_member": [
      "id",
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ],
    "roma_booking.rep_team_member": [
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ],
    "roma_planning.rep_team_member": [
      "id",
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ],
    "roma_call_history.rep_team_member": [
      "id",
      "code",
      "staff_code",
      "team_code",
      "team_role"
    ]
  },
  "roma_system.blacklist_keyword": {
    "roma_employer.rep_blacklist_keyword": [
      "id",
      "code",
      "channel_code",
      "title",
      "slug"
    ]
  },
  "roma_system.branch": {
    "roma_employer.rep_branch": [
      "id",
      "code",
      "channel_code",
      "province_codes"
    ],
    "roma_job.rep_branch": [
      "id",
      "code",
      "channel_code"
    ],
    "roma_sales_order.rep_branch": [
      "id",
      "code",
      "channel_code",
      "name",
      "bank_name",
      "bank_account_name",
      "bank_account_number",
      "active_status"
    ]
  },
  "roma_employer.employer": {
    "roma_job.rep_employer": [
      "id",
      "incremental_id",
      "code",
      "branch_code",
      "channel_code",
      "email",
      "name",
      "contact_phone",
      "contact_email",
      "address",
      "contact_address",
      "province_code",
      "account_status",
      "edit_status",
      "premium_status",
      "premium_renewed_at",
      "premium_created_at",
      "premium_end_at",
      "assigned_staff_code",
      "created_at",
      "last_logged_in_at",
      "cache_revision_status",
      "email_verified_status"
    ],
    "roma_booking.rep_employer": [
      "id",
      "code",
      "branch_code",
      "name",
      "email",
      "account_status",
      "incremental_id"
    ],
    "roma_call_history.rep_employer": [
      "id",
      "code",
      "branch_code",
      "name",
      "email",
      "account_status"
    ],
    "roma_job.job": {
      "join_src_table": "employer",
      "join_src_field": "code",
      "join_dst_field": "employer_code",
      "type": "object",
      "src_fields": [
        "incremental_id",
        "email",
        "name",
        "search_name",
        "address",
        "contact_address",
        "contact_email",
        "phone",
        "contact_phone",
        "assigned_staff_code",
        "account_status",
        "edit_status",
        "premium_status",
        "premium_end_at",
        "branch_code",
        "cache_revision_status",
        "email_verified_status"
      ],
      "dst_field": "cache_employer",
      "dst_field_indexes": [
        "incremental_id",
        "email",
        "name",
        "search_name",
        "contact_email",
        "phone",
        "contact_phone",
        "assigned_staff_code",
        "account_status",
        "edit_status",
        "premium_status",
        "premium_end_at",
        "branch_code",
        "cache_revision_status",
        "email_verified_status"
      ]
    },
    "roma_job.job_resume": {
      "join_src_table": "employer",
      "join_src_field": "code",
      "join_dst_field": "employer_code",
      "type": "object",
      "src_fields": [
        "assigned_staff_code"
      ],
      "dst_field": "cache_employer",
      "dst_field_indexes": [
        "assigned_staff_code"
      ]
    },
    "roma_sales_order.sales_order": {
      "join_src_table": "employer",
      "join_src_field": "code",
      "join_dst_field": "employer_code",
      "type": "object",
      "src_fields": [
        "incremental_id",
        "email",
        "name",
        "search_name",
        "assigned_staff_code",
        "branch_code",
        "email_verified_status",
        "total_remaining_buy_point",
        "total_remaining_gift_point"
      ],
      "dst_field": "cache_employer",
      "dst_field_indexes": [
        "incremental_id",
        "email",
        "name",
        "search_name",
        "assigned_staff_code",
        "branch_code"
      ]
    }
  },
  "roma_system.job_field": {
    "roma_job.rep_job_field": [
      "code",
      "channel_code",
      "name",
      "class_code",
      "active_status"
    ],
    "roma_sales_order.rep_job_field": [
      "code",
      "channel_code",
      "name",
      "active_status"
    ],
    "roma_booking.rep_job_field": [
      "id",
      "code",
      "parent_code",
      "channel_code",
      "class",
      "name",
      "active_status"
    ]
  },
  "roma_seeker.resume": {
    "roma_job.rep_resume": [
      "id",
      "code",
      "branch_code",
      "seeker_code",
      "title",
      "resume_type",
      "job_field_codes",
      "province_codes",
      "approved_status",
      "resume_status",
      "created_at",
      "created_by",
      "updated_at",
      "updated_by",
      "created_source"
    ],
    "roma_employer.employer_report_abuse": {
      "join_src_table": "resume",
      "join_src_field": "code",
      "join_dst_field": "resume_code",
      "type": "string",
      "src_fields": [
        "title"
      ],
      "dst_field": "cache_resume_title"
    },
    "roma_job.job_resume": {
      "join_src_table": "resume",
      "join_src_field": "code",
      "join_dst_field": "resume_code",
      "type": "object",
      "src_fields": [
        "title",
        "resume_type",
        "created_at",
        "updated_at",
        "job_field_codes",
        "province_codes",
        "resume_status"
      ],
      "dst_field": "cache_resume",
      "dst_field_indexes": [
        "title",
        "resume_type",
        "created_at",
        "updated_at",
        "resume_status"
      ]
    }
  },
  "roma_system.gate": {
    "roma_job.rep_gate": [
      "id",
      "code",
      "channel_code",
      "short_name",
      "full_name"
    ]
  },
  "roma_product.effect": {
    "roma_sales_order.rep_effect": [
      "id",
      "code",
      "name",
      "detail_part",
      "unit_price_percent",
      "active_status"
    ]
  },
  "roma_product.price_list": {
    "roma_sales_order.rep_price_list": [
      "id",
      "code",
      "title",
      "branch_code",
      "start_on",
      "end_on",
      "service_type",
      "active_status"
    ]
  },
  "roma_product.price_list_detail": {
    "roma_sales_order.rep_price_list_detail": [
      "id",
      "code",
      "price_list_code",
      "based_price",
      "unit_per_based"
    ]
  },
  "roma_product.price_list_promotion": {
    "roma_sales_order.rep_price_list_promotion": [
      "id",
      "code",
      "price_list_code",
      "promotion_rate",
      "discount_rate",
      "based_quantity",
      "point_week_quantity",
      "service_code",
      "unit_type"
    ]
  },
  "roma_product.service": {
    "roma_sales_order.rep_service": [
      "id",
      "code",
      "channel_code",
      "name",
      "service_type",
      "page_type",
      "active_status",
      "min_day_to_register",
      "max_day_to_register",
      "benefit_html",
      "benefit_html_new"
    ],
    "roma_booking.rep_service": [
      "id",
      "code",
      "channel_code",
      "name",
      "service_type",
      "page_type",
      "spot_limit",
      "active_status"
    ],
    "roma_booking.booking": {
      "join_src_table": "service",
      "join_src_field": "code",
      "join_dst_field": "service_code",
      "type": "string",
      "src_fields": [
        "name"
      ],
      "dst_field": "cache_service_name"
    }
  },
  "roma_system.displayed_region": {
    "roma_sales_order.rep_displayed_region": [
      "id",
      "code",
      "branch_codes",
      "discount_rate",
      "name",
      "active_status"
    ],
    "roma_booking.rep_displayed_region": [
      "id",
      "code",
      "branch_codes",
      "name",
      "active_status"
    ]
  },
  "roma_product.service_jobbox_field_spot_limit": {
    "roma_booking.rep_service_jobbox_field_spot_limit": [
      "id",
      "code",
      "channel_code",
      "service_code",
      "displayed_region_code",
      "job_field_code",
      "spot_limit"
    ]
  },
  "roma_system.calendar": {
    "roma_planning.rep_calendar": [
      "id",
      "code",
      "date",
      "week",
      "month",
      "quarter",
      "year",
      "holiday_note"
    ]
  },
  "roma_system.common_data": {
    "roma_planning.rep_common_data": [
      "id",
      "code",
      "channel_code",
      "type",
      "name",
      "value",
      "active_status"
    ]
  },
  "roma_employer.employer_revision": {
    "roma_employer.employer": {
      "join_src_table": "employer_revision",
      "join_src_field": "employer_code",
      "join_dst_field": "code",
      "type": "string",
      "src_fields": [
        "revision_status"
      ],
      "dst_field": "cache_revision_status"
    }
  },
  "roma_employer.employer_view_resume_history": {
    "roma_employer.employer": {
      "join_src_table": "employer_view_resume_history",
      "join_src_field": "employer_code",
      "join_dst_field": "code",
      "type": "string",
      "src_fields": [
        "resume_code"
      ],
      "dst_field": "cache_last_viewed_resume"
    }
  },
  "roma_seeker.seeker": {
    "roma_employer.employer_report_abuse": {
      "join_src_table": "seeker",
      "join_src_field": "code",
      "join_dst_field": "seeker_code",
      "type": "object",
      "src_fields": [
        "email",
        "name"
      ],
      "dst_field": "cache_seeker",
      "dst_field_indexes": []
    },
    "roma_employer.employer_view_resume_history": {
      "join_src_table": "seeker",
      "join_src_field": "code",
      "join_dst_field": "seeker_code",
      "type": "string",
      "src_fields": [
        "name"
      ],
      "dst_field": "cache_seeker_name",
      "dst_field_indexes": []
    }
  },
  "roma_booking.booking": {
    "roma_job.job": {
      "join_src_table": "booking",
      "join_src_field": "job_code",
      "join_dst_field": "code",
      "type": "string",
      "src_fields": [
        "cache_service_name"
      ],
      "dst_field": "cache_box_name"
    }
  },
  "roma_job.job_revision": {
    "roma_job.job": {
      "join_src_table": "job_revision",
      "join_src_field": "job_code",
      "join_dst_field": "code",
      "type": "string",
      "src_fields": [
        "revision_status"
      ],
      "dst_field": "cache_revision_status"
    }
  },
  "roma_sales_order.request_waiting_payment": {
    "roma_sales_order.sales_order": {
      "join_src_table": "request_waiting_payment",
      "join_src_field": "sales_order_code",
      "join_dst_field": "code",
      "type": "object",
      "src_fields": [
        "code",
        "request_type",
        "approval_status"
      ],
      "dst_field": "cache_waiting_payment",
      "dst_field_indexes": [
        "code",
        "request_type",
        "approval_status"
      ],
      "ignore_delete": true
    }
  },
  "roma_call_history.call_qa_evaluation_comment": {
    "roma_call_history.call_history": {
      "join_src_table": "call_qa_evaluation_comment",
      "join_src_field": "xlite_call_id",
      "join_dst_field": "xlite_call_id",
      "type": "object",
      "src_fields": [
        "qa_evaluation_status",
        "customer_care_evaluation_status",
        "member_feedback_status",
        "leader_feedback_status",
        "active_status",
        "created_at"
      ],
      "dst_field": "cache_call_qa_evaluation_comment",
      "dst_field_indexes": [
        "qa_evaluation_status",
        "customer_care_evaluation_status",
        "member_feedback_status",
        "leader_feedback_status",
        "active_status",
        "created_at"
      ]
    }
  }
}