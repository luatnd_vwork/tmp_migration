/**
 * Notice via slack
 */
const LOG_LEVEL = 1;
const CLIENT = 'mongo2mongo_nodejs_migration';
function getPrefix() {
  return `[${CLIENT}] `
}

function debug(message) {
  message = getPrefix + message;
}
function info(message) {
  message = getPrefix + message;
}
function warning(message) {
  message = getPrefix + message;
}
function error(message) {
  message = getPrefix + message;
}
function critical(message) {
  message = getPrefix + message;
}

module.exports = {
  debug: debug,
  info: info,
  warning: warning,
  error: error,
  critical: critical,
}
