const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_job',
};

const MongoClient = require('mongodb').MongoClient;

const fs = require('fs')
  , Log = require('log')
  , log = new Log('debug', fs.createWriteStream('logs/my.log'));


log.on('line', function(line){
  console.log(line);
});

execute();

function execute() {
  const TOTAL_COUNT = 2184227;

  MongoClient.connect(mongo.url, function (err, client) {

    log.info("Connected successfully to server");

    let from = 0;
    const size = 5000;
    const db = client.db(mongo.db);
    const col = db.collection('job');

    loop(from, size);


    async function loop(from, size) {

      const docs = await getJobItemList(col, from, size);

      if (docs === null) {
        log.error(`getJobItemList return null. Please fix it. ${col}, ${from}, ${size}`);
        return;
      }

      log.info('getJobItemList count: ', docs.length);

      // Get update data
      const data = [];
      docs.map((doc) => {
        const {
          email = null,
          name = null,
          search_name = null,
          contact_email = null,
          phone = null,
          contact_phone = null,
          assigned_staff_code = null,
          account_status = null,
          premium_status = null,
          premium_end_at = null,
        } = (typeof doc.employer[0] !== 'undefined') ? doc.employer[0] : {};

        data.push({
          find: { _id: doc._id },
          set: {
            cache_employer: {
              email,
              name,
              search_name,
              contact_email,
              phone,
              contact_phone,
              assigned_staff_code,
              account_status,
              premium_status,
              premium_end_at,
            }
          },
        });
      });

      const result = await batchUpdate(col, data);
      log.info('batchUpdate.result: ', result);


      if (from + size < TOTAL_COUNT) {
      	log.info('');
        log.info('Continue because from < TOTAL_COUNT: ' + (from + size) + ', ' + TOTAL_COUNT);
        setTimeout(() => loop(from + size, size), 250);
      }
    }

    console.log("Please see the: logs/my.log");

    // When to close db:
    // db.close();
  });
}

function getJobItemList(col, from, size) {
  return new Promise(resolve => {
    col.aggregate([
      { $skip: from },
      { $limit: size },
      {
        $lookup: {
          from: 'rep_employer',
          localField: 'employer_code',
          foreignField: 'code',
          as: 'employer',
        }
      },
      {
        $project: {
          _id: 1,
          employer_code: 1,
          employer: {
            email: 1,
            name: 1,
            search_name: 1,
            contact_email: 1,
            phone: 1,
            contact_phone: 1,
            assigned_staff_code: 1,
            account_status: 1,
            premium_status: 1,
            premium_end_at: 1,
          }
        }
    },
    ]).toArray(function (err, docs) {
      if (docs === null) {
        log.error('aggregate.toArray return null');
      }

      resolve(docs);

      //db.close();
    });
  });
}

function batchUpdate(col, data) {
  const batch = col.initializeUnorderedBulkOp({ useLegacyOps: true });

  // Add some operations to be executed in order
  data.map(updateData => {
    batch.find(updateData.find).updateOne({ $set: updateData.set });

    return true;
  });

  // Execute the operations
  return new Promise(resolve => {
    batch.execute(function (err, result) {
      resolve({
        success: true,
        error: err,
        //result: result,
      })
    });
  });
}