const { getRandItem } = require('./utils/Array');


let divisionDbData = require('./division.json');


const existData = {
  division: divisionDbData,
  channel: [{ code: 'TVN' }, { code: 'VTN' }, { code: 'VL24' }],
  service: [{ code: 'customer_care' }, { code: 'accountant' }, { code: 'customer_care' }],
};

const insertData = {
  action: [],
  permission: [],
  permission_denied: [],
};

const insertDataCount = {
  action: 300,
  permission: 10 * 200, // each division might have ~200 action
  permission_denied: 10*10,
};

const commonItemData = {
  active_status: 1,
  created_at: new Date(),
  updated_at: new Date(),
  created_by: null,
  updated_by: null,
  created_source: "luatnd-dev",
};



// Add action
for (let i = 1; i <= insertDataCount.action; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'ACT_' + numStr,
    action_group: 'action_group ' + numStr,
    name: 'act name ' + numStr,
    service: 'customer_care',
    controller: 'controller_' + numStr,
    action: 'action_' + numStr,
  };

  insertData.action.push({ ...item, ...commonItemData });
}

// Add permission
for (let i = 1; i <= insertDataCount.permission; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'PERM_' + numStr,
    division_code: getRandItem(existData.division).code,
    action_code: getRandItem(insertData.action).code,
  };

  insertData.permission.push({ ...item, ...commonItemData });
}
// Add permission_denied
for (let i = 1; i <= insertDataCount.permission_denied; i++) {
  const numStr = i.toString().padStart(6, "0"); // STAFF_000001

  const item = {
    code: 'PERM_D_' + numStr,
    channel_code: getRandItem(existData.channel).code,
    division_code: getRandItem(existData.division).code,
    action_code: getRandItem(insertData.action).code,
  };

  insertData.permission_denied.push({ ...item, ...commonItemData });
}


//console.log('insertData: ', insertData);
//process.exit();


var fs = require('fs');
fs.writeFile(
  './src/actionData.json',
  JSON.stringify(insertData, null, 2),
  function (err) {
    if (err) throw err;
    console.log('Saved to ./actionData.json');
  }
);

console.log('Finished and exit');


module.exports = insertData;

