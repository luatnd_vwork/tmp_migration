const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_auth',
};

saveAllDivisionToJson();


function saveAllDivisionToJson() {
  MongoClient.connect(mongo.url, function(err, db) {
    if (err) throw err;
    const dbo = db.db(mongo.db);

    dbo.collection("division").find({}).toArray(function(err, result) {
      if (err) throw err;

      let divisionDbData = result;

      db.close();


      fs.writeFile(
        './src/division.json',
        JSON.stringify(divisionDbData, null, 2),
        function (err) {
          if (err) throw err;

          console.log('Saved to ./division.json');
        }
      );

    });

  });
}

