const fs = require('fs');
const MongoClient = require('mongodb').MongoClient;
const mongo = {
  url: 'mongodb://206.189.150.42:27017',
  db: 'roma_auth',
};
const collectionName = "menu";


saveAllItemToJson();


function saveAllItemToJson() {
  MongoClient.connect(mongo.url, function(err, db) {
    if (err) throw err;
    const dbo = db.db(mongo.db);

    dbo.collection(collectionName).find({}).toArray(function(err, result) {
      if (err) throw err;

      let items = result;

      db.close();


      fs.writeFile(
        `./src/${collectionName}.json`,
        JSON.stringify(items, null, 2),
        function (err) {
          if (err) throw err;

          console.log(`Saved to ./${collectionName}.json`);
        }
      );

    });

  });
}

