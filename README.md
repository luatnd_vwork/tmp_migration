Prerequisite:
```
# nodejs v8
$ node -v
v8.11.1

$ npm -v
6.0.0
```


How to build and run

```
yarn install
node src/update2mongo/job__cache_employer.js 0 20000
```
